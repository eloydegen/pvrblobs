This repository contains all the vendor blobs from a Colorfly E708 Q1 tablet

The goal of the project is integrating them with libhybris in PostmarketOS


```
root@fiber-colorfly:/ # lsmod
8188eu 842755 0 - Live 0x00000000
ilitek_aimvF 61308 0 - Live 0x00000000
stk831x 27785 0 - Live 0x00000000
rtl8150 8522 0 - Live 0x00000000
mcs7830 5847 0 - Live 0x00000000
qf9700 7328 0 - Live 0x00000000
asix 16337 0 - Live 0x00000000
sunxi_temperature 3958 0 - Live 0x00000000
sw_keyboard 2660 0 - Live 0x00000000
sw_device 11063 0 - Live 0x00000000
g2d_33 37741 0 - Live 0x00000000
vfe_v4l2 192474 0 - Live 0x00000000
gc0328 8623 1 - Live 0x00000000
vfe_subdev 4514 2 vfe_v4l2,gc0328, Live 0x00000000
vfe_os 2196 2 vfe_v4l2,vfe_subdev, Live 0x00000000
cci 3061 1 gc0328, Live 0x00000000
cam_detect 31901 1 vfe_v4l2, Live 0x00000000
videobuf_dma_contig 5322 1 vfe_v4l2, Live 0x00000000
videobuf_core 15891 2 vfe_v4l2,videobuf_dma_contig, Live 0x00000000
dc_sunxi 7136 1 - Live 0x00000000 (O)
pvrsrvkm 295721 86 dc_sunxi, Live 0x00000000 (O)
hdmi 24942 0 - Live 0x00000000
lcd 13786 0 - Live 0x00000000
disp 1050142 9 dc_sunxi,hdmi,lcd, Live 0x00000000
nand 173611 8 - Live 0x00000000 (O)
```
