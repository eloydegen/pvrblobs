```./pvrsrvctl: must specify mode of operation
Usage: ./pvrsrvctl [OPTION]
Load and initialize (or unload) PowerVR services.

  --start	start PowerVR services (may load kernel module)
  --stop	stop PowerVR services (may unload kernel module)
  --restart	stops and starts PowerVR services
  --no-module	suppresses kernel module load/unload
  --no-init	suppresses services initialization
  --dump-debug	dumps SGX debug information to dmesg
  --force-regs	if specified with --dump-debug, dumps registers
  --help	display this help and exit```
